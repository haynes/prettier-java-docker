FROM node:16-alpine

ARG BUILD_DATE
ARG VCS_REF
ARG PRETTIER_VERSION="2.5.0"
ARG PRETTIERJAVA_VERSION="1.6.0"

LABEL \
      # https://github.com/opencontainers/image-spec/blob/master/annotations.md
      "org.opencontainers.image.created"="${BUILD_DATE}" \
      "org.opencontainers.image.authors"="Hannes Rosenögger <123haynes@gmail.com>" \
      "org.opencontainers.image.homepage"="https://gitlab.com/haynes/prettier-java-docker" \
      "org.opencontainers.image.documentation"="https://gitlab.com/haynes/prettier-java-docker/README.md" \
      "org.opencontainers.image.source"="https://gitlab.com/haynes/prettier-java-docker" \
      "org.opencontainers.image.version"="prettier version ${PRETTIER_VERSION} and prettier-java version ${PRETTIERJAVA_VERSION}" \
      "org.opencontainers.image.revision"="${VCS_REF}" \
      "org.opencontainers.image.title"="ci-prettier" \
      "org.opencontainers.image.description"="Prettier in a docker container for linting and formatting various files types"

RUN apk add --no-cache git

RUN npm install -g prettier@${PRETTIER_VERSION} prettier-plugin-java@${PRETTIERJAVA_VERSION} @studiometa/prettier-formatter-gitlab @prettier/plugin-xml prettier-plugin-properties
