# prettier-java-docker

This project provides a small docker image with:  
* the latest LTS version of [nodejs](https://nodejs.org/)  
* [prettier](https://github.com/prettier/prettier)  
* [prettier-java](https://github.com/jhipster/prettier-java)  
* [Prettier formatter for GitLab Code Quality](https://www.npmjs.com/package/@studiometa/prettier-formatter-gitlab)  
* [Prettier for XML](https://github.com/prettier/plugin-xml)  
* [Prettier for .properties files](https://github.com/eemeli/prettier-plugin-properties)

# example usage

```yml
prettier-validation:
  stage: build
  script:
    - prettier-formatter-gitlab 'prettier --check "**/*.java"'
  artifacts:
    reports:
      codequality: gl-codequality.json
```

# Important

You need a `.prettierrc` file in your project root.   
See https://prettier.io/docs/en/configuration.html for examples.  
Otherwise `prettier-formatter-gitlab` will throw an error and not work correctly

# Prepare your project:

Add the following `package.json` to your project:  
```json
{
  "devDependencies": {
    "@prettier/plugin-xml": "^1.1.0",
    "husky": "^7.0.0",
    "prettier": "^2.5.0",
    "prettier-plugin-java": "^1.6.0",
    "prettier-plugin-properties": "^0.1.0",
    "pretty-quick": "^3.1.2"
  },
  "scripts": {
    "prepare": "husky install && husky add .husky/pre-commit \"npx pretty-quick --staged\""
  }
}
```

Execute `npm install` to set everything up on your machine.
This will create a pre-commit hook which will run prettier for all staged files.